今天试着做一个稍微有点难度的组件，轮播图。

轮播图的按钮部分可以复用下昨天写的radio。



## 组件分析

轮播图主要还是做出来，功能可以不用特别全。

最起码的功能就是自动播放和翻页了。

轮播制作原理使用姜老师的图片拼接转移。就是在翻页时把需要的图片拼起来然后进行移动即可。

### 逻辑分析

由于这个拼图逻辑略微复杂，给大家梳理下。

一个能正常拼图显示的轮播需要2个元素宽就可以，但是如果要做动画，也就是需要3个元素宽。

中间那个确保显示，如果要往左边移，那么要在右边拼一张将要显示的元素然后左移。

如果要往右移，那么要在左边拼一张将要显示的元素然后右移。

这个过程，我们可以结合react的state特性进行制作。

只要我们点击切换按钮，追加要显示的元素，并让元素显示出正确的顺序，再进行动画，移动到正常的位置即可。

## 初步实现

为了实现上面那个效果，我们需要存几个状态。

第一个状态就是要渲染的元素，由于是3个元素构成完整屏幕，所以这个状态做成长度为3的数组。

```
const [state, setState] = useState<ReactNode[]>([]);
```

第二个状态是索引map，对应上面那个状态的应显示vnode的索引。而-1代表无索引。

```
const [indexMap, setIndexMap] = useState<[number, number, number]>([
		-1,
		-1,
		-1,
	]);
```

第三个状态用来根据索引判断动画方向。

```
	const [animation, setAnimation] = useState<AnimationType>({
		animatein: true,
		direction: "",
	});
```

第4个状态用来制作宽度。因为页面宽度可能是不固定的，所以我们需要进行宽度自适应。高度就需要使用的人给了。

```
	const [bound, setBound] = useState<DOMRect>();
```

对于styledcomponents，没必要特别去使用这玩意，可以先用原生写出来，然后对于需要控制的css，转换成styledcomponents组件。

```typescript
const Transition = styled.div<AnimationType>`
	${(props) =>
		!props.animatein &&
		props.direction === "left" &&
		`
		transform: translateX(100%);
		`}
	${(props) =>
		!props.animatein &&
		props.direction === "right" &&
		`
		transform: translateX(-100%);
	
		`}
	${(props) =>
		props.animatein &&
		props.direction === "left" &&
		`
		transform: translateX(0);
			transition: all 1s ease;
		`}
	${(props) =>
		props.animatein &&
		props.direction === "right" &&
		`
		transform: translateX(0);
		transition: all 1s ease;
		`}
		
	
`;

type CarouselProps = {
	/** 默认索引*/
	defaultIndex?: number;
	/** 轮播图高度 */
	height?: number;
	/** 是否自动播放 */
	autoplay: boolean;
	/** 自动播放延迟 */
	autoplayDelay: number;
	/** 翻页动画延迟 */
	delay?: number;
};

function currentSetMap(
	current: number,
	map: [number, number, number]
): [number, number, number] {
	let mid = map[1];
	if (mid === current) {
		return map;
	} else if (mid < current) {
		return [mid, current, -1];
	} else {
		return [-1, current, mid];
	}
}
function mapToState(
	map: [number, number, number],
	children: ReactNode,
	totalLen: number
) {
	if (totalLen <= 1) {
		return [null, children, null];
	} else {
		return map.map((v) => {
			if (v === -1) {
				return null;
			} else {
				let child = children as ReactElement[];
				return child[v];
			}
		});
	}
}
interface AnimationType {
	animatein: boolean;
	direction: "" | "left" | "right";
}

export function Carousel(props: PropsWithChildren<CarouselProps>) {
	const { defaultIndex, height, autoplayDelay, delay } = props;
	//设置需要展示的元素
	const [state, setState] = useState<ReactNode[]>([]);
	//设置显示索引用
	const [indexMap, setIndexMap] = useState<[number, number, number]>([
		-1,
		-1,
		-1,
	]);
	//控制方向进出用
	const [animation, setAnimation] = useState<AnimationType>({
		animatein: true,
		direction: "",
	});
	//设置宽度用
	const [bound, setBound] = useState<DOMRect>();
	const totalLen = useMemo(() => {
		let len: number;
		if (props.children instanceof Array) {
			len = props.children.length;
		} else {
			len = 1;
		}
		return len;
	}, [props.children]);
	useMemo(() => {
		let map: [number, number, number] = [-1, -1, -1];
		map[1] = defaultIndex!;
		let res = mapToState(map, props.children, totalLen);
		setState(res);
		setIndexMap(map);
	}, [defaultIndex, props.children, totalLen]);
	useEffect(() => {
		let child = children as ReactElement[];
		let timer: number;
		if (child) {
			let tmp = indexMap.map((v) => {
				return v !== -1 ? child[v] : null;
			});
			let sign: boolean;
			setState(tmp); //后setState会有补足问题必须先设

			if (indexMap[0] === -1 && indexMap[2] === -1) {
				//首轮
				return;
			} else if (indexMap[0] === -1) {
				sign = true;
				setAnimation({ animatein: false, direction: "right" });
			} else {
				sign = false;
				setAnimation({ animatein: false, direction: "left" });
			}
			timer = window.setTimeout(() => {
				if (sign) {
					setAnimation({ animatein: true, direction: "right" });
				} else {
					setAnimation({ animatein: true, direction: "left" });
				}
			}, delay!);
		}
		return () => window.clearTimeout(timer);
	}, [delay, indexMap, children]);
	const ref = useRef<HTMLDivElement>(null);
	useEffect(() => {
		const setBoundFunc = () => {
			if (ref.current) {
				let bounds = ref.current.getBoundingClientRect();
				setBound(bounds);
			}
		};
		setBoundFunc();
		const resizefunc = () => {
			setBoundFunc();
		};
		window.addEventListener("resize", resizefunc);
		return () => {
			window.removeEventListener("resize", resizefunc);
		};
	}, []);
	return (
		<div ref={ref}>
			<div
				className="viewport"
				style={{ width: `100%`, height: `${height!}px` }}
			>
				<Transition
					animatein={animation.animatein}
					direction={animation.direction}
				>
					<div
						style={{
							display: "flex",
							width: `${bound?.width! * 3}px`,
							position: "absolute",
							left: `${-bound?.width!}px`,
						}}
					>
						{state.map((v, i) => (
							<div
								key={i}
								style={{
									height: `${height!}px`,
									width: `${bound?.width}px`,
								}}
							>
								{v}
							</div>
						))}
					</div>
				</Transition>
			</div>
			<ul
				style={{
					display: "flex",
					justifyContent: "center",
					alignItems: "center",
				}}
			>
				{new Array(totalLen).fill(1).map((x, y) => {
					return (
						<Radio
							label=""
							key={y}
							hideLabel
							value={0}
							checked={y === indexMap[1]}
							onChange={() => {}}
							onClick={() => {
								let newmap = currentSetMap(y, indexMap);
								setIndexMap(newmap);
							}}
						/>
					);
				})}
			</ul>
		</div>
	);
}
Carousel.defaultProps = {
	defaultIndex: 0,
	delay: 100,
	height: 200,
	autoplay: true,
	autoplayDelay: 5000,
};
```

这里我用styledcomponents制作了transition来控制动画。

复用了昨天写的radio进行控制翻页。

可以在story处做个story查看：

```typescript
export const knobsCarousel = () => (
	<div>
		<Carousel autoplay delay={1000} height={300}>
			<div style={{ height: "100%", width: "100%", background: "red" }}>
				1
			</div>
			<div style={{ height: "100%", width: "100%", background: "blue" }}>
				2
			</div>
			<div
				style={{ height: "100%", width: "100%", background: "yellow" }}
			>
				3
			</div>
			<div style={{ height: "100%", width: "100%" }}>4</div>
		</Carousel>
	</div>
);
```

## 增加功能

前面已经基本实现了轮播图的效果，这时只要增加overflow:hidden就跟基本的轮播图差不多了。我们需要给viewport上加上这属性。

只加hidden不够，由于hidden会找一个定位元素，所以还需要将viewport设置为relative。否则会出现滚动条。

```
<div
				className="viewport"
				style={{
					width: `100%`,
					height: `${height!}px`,
					overflow: "hidden",
					position: "relative",
				}}
			>
```

对于动画的时间，也应该增加功能。暴露动画配置时间，然后动态改它。同时需要将其类型修改下：

```typescript
type CarouselProps = {
	/** 默认索引*/
	defaultIndex?: number;
	/** 轮播图高度 */
	height?: number;
	/** 是否自动播放 */
	autoplay: boolean;
	/** 自动播放延迟 */
	autoplayDelay: number;
	/** 翻页动画延迟 */
	delay?: number;
	/**  动画速度 1000是1秒 */
	animationDelay?: number;
};

```



```typescript
interface TransitionType extends AnimationType {
	delay: number;
}
const Transition = styled.div<TransitionType>`
	${(props) =>
		!props.animatein &&
		props.direction === "left" &&
		`
		transform: translateX(100%);
		`}
	${(props) =>
		!props.animatein &&
		props.direction === "right" &&
		`
		transform: translateX(-100%);
	
		`}
	${(props) =>
		props.animatein &&
		props.direction === "left" &&
		`
		transform: translateX(0);
			transition: all ${props.delay / 1000}s ease;
		`}
	${(props) =>
		props.animatein &&
		props.direction === "right" &&
		`
		transform: translateX(0);
		transition: all ${props.delay / 1000}s ease;
		`}
`;

```

```typescript
				<Transition
					animatein={animation.animatein}
					direction={animation.direction}
					delay={animationDelay!}
				>
```

我们还需要制作autoplay功能：

有了上面代码，制作autoplay就很简单了，增加个useEffect就可以了，另外，这个移动功能可能要给其他dom使用，所以把它封装成函数：

```typescript
function toMove(
	right: boolean,
	totalLen: number,
	indexMap: [number, number, number],
	setIndexMap: React.Dispatch<React.SetStateAction<[number, number, number]>>
) {
	let y;
	if (right) {
		if (indexMap[1] < totalLen - 1) {
			y = indexMap[1] + 1;
		} else {
			y = 0;
		}
	} else {
		if (indexMap[1] === 0) {
			y = totalLen - 1;
		} else {
			y = indexMap[1] - 1;
		}
	}
	setIndexMap(currentSetMap(y, indexMap));
}
```

在useEffect里调用就行了。

```typescript
		useEffect(() => {
		let timer: number;
		if (autoplay) {
			timer = window.setTimeout(() => {
				toMove(true, totalLen, indexMap, setIndexMap);
			}, autoplayDelay);
		}
		return () => window.clearTimeout(timer);
	}, [autoplay, autoplayDelay, indexMap, totalLen]);
```

hooks真的是非常好用，可以把逻辑都写一起。

有了这个函数，我们还可以暴露是否反向自动播放：

```
export type CarouselProps = {
	....
	....
	...
	/**自动播放时是否反向播放 */
	autoplayReverse?: boolean;
};
```

把刚才函数改写下，别忘记添加依赖：

```typescript
	if (autoplay) {
			timer = window.setTimeout(() => {
				toMove(!autoplayReverse!, totalLen, indexMap, setIndexMap);
			}, autoplayDelay);
		}
```

我们还需要搞个radio属性配置，我们做的那个radio在这里除了颜色，其它功能没啥用，用了也不好看，所以就配个颜色：

```
export type CarouselProps = {
	...
	...
	...
	/** radio color */
	radioAppear?: keyof typeof color;
};
```

radio里加入这个配置：

```typescript
<Radio
							label=""
							appearance={radioAppear}
							key={y}
							hideLabel
							value={0}
							checked={y === indexMap[1]}
							onChange={() => {}}
							onClick={() => {
								let newmap = currentSetMap(y, indexMap);
								setIndexMap(newmap);
							}}
						/>
```

为了使得移动端体验良好，我们需要给viewport绑定touchstart与touchend事件。

这个功能我们需要存一个起始坐标，在点击的结束坐标里相减，得到相差值。

对于这个差值，可以暴露出来作为一个配置，同时别忘记设置初始值。

```typescript
	const [start, setStart] = useState(0);
	const touchStart = (e: TouchEvent<HTMLDivElement>) => {
		setStart(e.touches[0].clientX);
	};
	const touchEnd = (e: TouchEvent<HTMLDivElement>) => {
		let end = e.changedTouches[0].clientX;
		let val = end - start;
		let abs = Math.abs(val);
		if (abs > touchDiff!) {
			//说明可以进一步判断
			if (val > 0) {
				//从左往右 向左翻
				toMove(false, totalLen, indexMap, setIndexMap);
			} else {
				toMove(true, totalLen, indexMap, setIndexMap);
			}
		}
	};
```

这样功能基本差不多完成了，下面稍微进行美化下

## 美化工作

怎么美化可以自由发挥，最好需要把wrapper上的style 和classname暴露出来，这样以后使用的时候会比较方便改样式和定位。同时可以把前面加的classname给去了，那个是为了方便调试加上的。

我将最外层div换成styledcomponent，同时美化些样式：

```typescript
interface WrapperProps {
	viewportBoxshadow: string;
}

const Wrapper = styled.div<WrapperProps>`
	box-shadow: ${(props) => props.viewportBoxshadow};
	padding: 10px;
	border-radius: 5px;
`;
```

可配置box-shadow：

````typescript
	<Wrapper
			ref={ref}
			style={style}
			className={classname}
			viewportBoxshadow={viewportBoxshadow!}
		>
			<div
				style={{
					width: `100%`,
					height: `${height!}px`,
					overflow: "hidden",
					position: "relative",
					borderRadius: "10px",
					boxShadow: viewportBoxshadow,
				}}
				onTouchStart={touchStart}
				onTouchEnd={touchEnd}
			>
````

注意，这里美化会有个坑，就是加了padding后，translate会校不准，因为我们translateX是用的100%，所以padding会有影响。这时，可以将width传给它，直接使用width进行位移即可。

```typescript
const Transition = styled.div<TransitionType>`
	${(props) =>
		!props.animatein &&
		props.direction === "left" &&
		`
		transform: translateX(${props.width}px);
		`}
	${(props) =>
		!props.animatein &&
		props.direction === "right" &&
		`
		transform: translateX(${-props.width}px);
	
		`}
	${(props) =>
		props.animatein &&
		props.direction === "left" &&
		`
		transform: translateX(0);
			transition: all ${props.delay / 1000}s ease;
		`}
	${(props) =>
		props.animatein &&
		props.direction === "right" &&
		`
		transform: translateX(0);
		transition: all ${props.delay / 1000}s ease;
		`}
`;
```

这样就完成了

## 编写story

做测试的那个示例有点丑，可以仿照antd的示例，里面元素这么修改：

````typescript
const DivExample = function(height: number, index: number) {
	return (
		<div
			style={{
				background: "#364d79",
			}}
    		key={index}
		>
			<span
				style={{
					lineHeight: `${height}px`,
					color: "white",
					fontSize: "20px",
					fontWeight: 800,
					width: "100%",
					textAlign: "center",
					display: "inline-block",
				}}
			>
				{index + 1}
			</span>
		</div>
	);
};

````

然后示例里面加入：

```typescript
export const knobsCarousel = () => (
	<div>
		<Carousel delay={300} height={300} radioAppear="darker">
			{new Array(4).fill(300).map((v, i) => DivExample(v, i))}
		</Carousel>
	</div>
);
```

同时用knob进配置下轮播图参数：

```typescript
export const knobsCarousel = () => {
	const height = number("height", 300);
	const num = number("item number", 4);
	return (
		<Carousel
			delay={number("delay", 300)}
			height={height}
			radioAppear={select(
				"radioAppear",
				Object.keys(color) as Array<keyof typeof color>,
				"primary"
			)}
			defaultIndex={number("defaultIndex", 0)}
			autoplay={boolean("autoplay", true)}
			viewportBoxshadow={text("viewportBoxshadow", "2px 2px 4px #d9d9d9")}
			autoplayReverse={boolean("autoplayReverse", false)}
			animationDelay={number("animationDelay", 500)}
			autoplayDelay={number("autoplayDelay", 5000)}
		>
			{new Array(num).fill(height).map((v, i) => DivExample(v, i))}
		</Carousel>
	);
};
```

为了过story上的a11y检测，我稍微对结构进行了些调整和加了点属性，这个就不写了。

轮播图没啥额外需要展示的，写个knob基本差不多了。

## 编写测试

轮播图的测试比较多，这里需要各种测试方法搭配，但仍会有未测到的功能，就是jest不太好模拟内部ref，导致部分ref相关代码未覆盖到。

```typescript
import React from "react";
import { fireEvent, act, render as trender } from "@testing-library/react";
import { Carousel } from "../index";
import { render, unmountComponentAtNode } from "react-dom";

const testAutoplay = function() {
	return (
		<Carousel height={300} autoplayDelay={1000}>
			<span id="test1">1</span>
			<span id="test2">2</span>
			<span id="test3">3</span>
		</Carousel>
	);
};

const testReverseAuto = function() {
	return (
		<Carousel height={300} autoplayDelay={1000} autoplayReverse={true}>
			<span id="test1">1</span>
			<span id="test2">2</span>
			<span id="test3">3</span>
		</Carousel>
	);
};

const testHeight = function() {
	return (
		<Carousel height={100} autoplay={false}>
			<span id="test1">1</span>
			<span id="test2">2</span>
			<span id="test3">3</span>
		</Carousel>
	);
};

const testDefaultIndex = function() {
	return (
		<Carousel height={100} autoplay={false} defaultIndex={2}>
			<span id="test1">1</span>
			<span id="test2">2</span>
			<span id="test3">3</span>
		</Carousel>
	);
};

const testRadioColor = function() {
	return (
		<div>
			<Carousel height={100} autoplay={false} radioAppear="positive">
				<span id="test1">1</span>
				<span id="test2">2</span>
				<span id="test3">3</span>
			</Carousel>
			<Carousel height={100} autoplay={false}>
				<span id="test1">1</span>
				<span id="test2">2</span>
				<span id="test3">3</span>
			</Carousel>
			<Carousel height={100} autoplay={false} radioAppear="purple">
				<span id="test1">1</span>
				<span id="test2">2</span>
				<span id="test3">3</span>
			</Carousel>
		</div>
	);
};

const testTouch = function() {
	return (
		<Carousel height={100} autoplay={false}>
			<span id="test1" style={{ height: "100%", width: "100%" }}>
				1
			</span>
			<span id="test2" style={{ height: "100%", width: "100%" }}>
				2
			</span>
			<span id="test3" style={{ height: "100%", width: "100%" }}>
				3
			</span>
		</Carousel>
	);
};
const testOneItem = function() {
	return (
		<Carousel height={100} autoplay={false}>
			<span id="test1" style={{ height: "100%", width: "100%" }}>
				1
			</span>
		</Carousel>
	);
};

const testResize = function() {
	return (
		<Carousel height={100} autoplay={false}>
			<span id="test1" style={{ height: "100%", width: "100%" }}>
				1
			</span>
			<span id="test2" style={{ height: "100%", width: "100%" }}>
				2
			</span>
			<span id="test3" style={{ height: "100%", width: "100%" }}>
				3
			</span>
		</Carousel>
	);
};

const sleep = (delay: number) => {
	return new Promise((res) => {
		setTimeout(() => {
			res();
		}, delay);
	});
};

let container: HTMLDivElement;
beforeEach(() => {
	// 创建一个 DOM 元素作为渲染目标
	container = document.createElement("div");
	document.body.appendChild(container);
});
afterEach(() => {
	// 退出时进行清理
	unmountComponentAtNode(container);
});

describe("test Carousel component", () => {
	it("it should autoplay ", async () => {
		act(() => {
			render(testAutoplay(), container);
		});
		//默认索引是0
		let text1 = container.querySelector("#test1");
		let text2 = container.querySelector("#test2");
		let text3 = container.querySelector("#test3");
		//初始环境，只有1能看见
		expect(text1?.textContent).toEqual("1");
		expect(text2).toBeNull();
		expect(text3).toBeNull();
		//自动播放速度为5000，jest超过5000的定时器直接报错,所以设置1000的定时器
		await act(async () => {
			await sleep(1000);
		});
		text2 = container.querySelector("#test2");
		expect(text2?.textContent).toEqual("2");
		text3 = container.querySelector("#test3");
		expect(text3).toBeNull(); //此时3还没来，
		await act(async () => {
			await sleep(1000);
		});
		text3 = container.querySelector("#test3");
		expect(text3?.textContent).toEqual("3");
		text1 = container.querySelector("#test1");
		expect(text1).toBeNull();
		//3后面直接跳1
		await act(async () => {
			await sleep(1000);
		});
		text1 = container.querySelector("#test1");
		expect(text1?.textContent).toEqual("1");
		text2 = container.querySelector("#test2");
		expect(text2).toBeNull();
	});
	it("should autoplay reverse", async () => {
		act(() => {
			render(testReverseAuto(), container);
		});
		let text1 = container.querySelector("#test1");
		let text2 = container.querySelector("#test2");
		let text3 = container.querySelector("#test3");
		//初始环境，只有1能看见
		expect(text1?.textContent).toEqual("1");
		expect(text2).toBeNull();
		expect(text3).toBeNull();
		await act(async () => {
			await sleep(1000);
		});
		//直接倒退到3
		text3 = container.querySelector("#test3");
		expect(text3).toBeTruthy();
		text2 = container.querySelector("#test2");
		expect(text2).toBeNull();
		//然后是2
		await act(async () => {
			await sleep(1000);
		});
		text2 = container.querySelector("#test2");
		expect(text2).toBeTruthy();
		text1 = container.querySelector("#test1");
		expect(text1).toBeNull();
		//1
		await act(async () => {
			await sleep(1000);
		});
		text1 = container.querySelector("#test1");
		expect(text1).toBeTruthy();
		text3 = container.querySelector("#test3");
		expect(text3).toBeNull();
	});
	it("should render correct height", () => {
		act(() => {
			render(testHeight(), container);
		});
		let text1 = container.querySelector("#test1");
		expect(text1?.parentElement).toHaveStyle("height:100px");
	});
	it("should render correct default index", () => {
		act(() => {
			render(testDefaultIndex(), container);
		});
		let text3 = container.querySelector("#test3");
		expect(text3).toBeTruthy();
		let text1 = container.querySelector("#test1");
		expect(text1).toBeNull();
	});
	it("should render correct when click radio", () => {
		act(() => {
			render(testDefaultIndex(), container);
		});
		const label = container.getElementsByTagName("label");
		fireEvent.click(label[0]);
		let text1 = container.querySelector("#test1");
		let text2 = container.querySelector("#test2");
		let text3 = container.querySelector("#test3");
		expect(text1).toBeTruthy(); //默认3，3到1
		expect(text2).toBeNull();
		expect(text3).toBeTruthy();
		fireEvent.click(label[1]); //跳2
		text1 = container.querySelector("#test1");
		text2 = container.querySelector("#test2");
		text3 = container.querySelector("#test3");
		expect(text1).toBeTruthy();
		expect(text2).toBeTruthy();
		expect(text3).toBeNull();
		//点击相同
		fireEvent.click(label[1]); //跳2
		text1 = container.querySelector("#test1");
		text2 = container.querySelector("#test2");
		text3 = container.querySelector("#test3");
		expect(text1).toBeTruthy();
		expect(text2).toBeTruthy();
		expect(text3).toBeNull();
	});
	it("should render correct radio style", () => {
		const wrapper = trender(testRadioColor());
		expect(wrapper).toMatchSnapshot();
	});
	it("should trun the page when touch", () => {
		act(() => {
			render(testTouch(), container);
		});
		let text1 = container.querySelector("#test1");
		let text2 = container.querySelector("#test2");
		fireEvent.touchStart(text1!, { touches: [{ clientX: 20 }] });
		fireEvent.touchEnd(text1!, { changedTouches: [{ clientX: 130 }] });
		//向前翻1-3
		let text3 = container.querySelector("#test3");
		text2 = container.querySelector("#test2");
		expect(text3).toBeTruthy();
		expect(text2).toBeNull();
		//3-》2
		fireEvent.touchStart(text3!, { touches: [{ clientX: 20 }] });
		fireEvent.touchEnd(text3!, { changedTouches: [{ clientX: 130 }] });
		text2 = container.querySelector("#test2");
		expect(text2).toBeTruthy();
		text1 = container.querySelector("#test1");
		expect(text1).toBeNull();
		//2-1
		fireEvent.touchStart(text2!, { touches: [{ clientX: 20 }] });
		fireEvent.touchEnd(text2!, { changedTouches: [{ clientX: 130 }] });
		text1 = container.querySelector("#test1");
		expect(text1).toBeTruthy();
		text3 = container.querySelector("#test3");
		expect(text3).toBeNull();
		//向后翻 1-》2
		fireEvent.touchStart(text1!, { touches: [{ clientX: 130 }] });
		fireEvent.touchEnd(text1!, { changedTouches: [{ clientX: 20 }] });
		text1 = container.querySelector("#test1");
		expect(text1).toBeTruthy();
		text2 = container.querySelector("#test2");
		expect(text2).toBeTruthy();
		text3 = container.querySelector("#test3");
		expect(text3).toBeNull();
		//2-3
		fireEvent.touchStart(text2!, { touches: [{ clientX: 130 }] });
		fireEvent.touchEnd(text2!, { changedTouches: [{ clientX: 20 }] });
		text1 = container.querySelector("#test1");
		expect(text1).toBeNull();
		text2 = container.querySelector("#test2");
		expect(text2).toBeTruthy();
		text3 = container.querySelector("#test3");
		expect(text3).toBeTruthy();
		//不变
		fireEvent.touchStart(text2!, { touches: [{ clientX: 130 }] });
		fireEvent.touchEnd(text2!, { changedTouches: [{ clientX: 120 }] });
		text1 = container.querySelector("#test1");
		expect(text1).toBeNull();
		text2 = container.querySelector("#test2");
		expect(text2).toBeTruthy();
		text3 = container.querySelector("#test3");
		expect(text3).toBeTruthy();
	});
	it("should render one item", () => {
		const wrapper = trender(testOneItem());
		expect(wrapper).toMatchSnapshot();
	});
	it("should test resize", async () => {
		act(() => {
			render(testResize(), container);
		});
		act(() => {
			window.dispatchEvent(new Event("resize"));
		});
		expect(container).toMatchSnapshot();
	});
});
```

## 今日作业

完成轮播图组件

