import React from "react";
import { Button } from "roshanui";
import { GlobalStyle } from "roshanui";

function App() {
  return (
    <div className="App">
      <GlobalStyle></GlobalStyle>
      <Button appearance="primary">2222</Button>
    </div>
  );
}

export default App;