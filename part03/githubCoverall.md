## 1.coveralls注册
- 访问[coveralls](https://coveralls.io/)
- 注册并登录，同步后使用add repo添加已上传项目

![](http://img.zhufengpeixun.cn/1.addrepo.png)

点击detail到详情页拷贝出来token

![](http://img.zhufengpeixun.cn/2.copytoken.png)


## 2.添加COVERALLS_REPO_TOKEN
- 切回[github仓库](https://github.com/zhufengnodejs/zhufengreactui/settings)
- 在[secrets](https://github.com/zhufengnodejs/zhufengreactui/settings/secrets)中添加一个secret,名称为`COVERALLS_REPO_TOKEN`,值为第1步拷贝出来的token

![](http://img.zhufengpeixun.cn/3.COVERALLS_REPO_TOKEN.png)

## 3.安装coveralls
```js
yarn add coveralls
```

## 4.添加命令
```diff
  "scripts": {
    "start": "react-scripts start",
    "build": "tsc -p tsconfig.build.json",
    "test": "react-scripts test",
    "eject": "react-scripts eject",
    "storybook": "start-storybook -p 6006 -s public",
    "build-storybook": "build-storybook --no-dll --quiet",
    "coverage": "react-scripts test --coverage --watchAll=false",
+    "coverall": "npm run coverage  && cat ./coverage/lcov.info | ./node_modules/coveralls/bin/coveralls.js && rm -rf ./coverage"
  },
```

## 5.修改githubActions
.github\workflows\你的名字.yml
```diff
+  - name: Coveralls
+        env:
+          COVERALLS_SERVICE_NAME: 'GitHub CI'
+          COVERALLS_GIT_BRANCH: master
+          COVERALLS_REPO_TOKEN : ${{secrets.COVERALLS_REPO_TOKEN}}
+        run: |
+          npm run coverall
```

## 6.添加badges
- 把这一段拷贝到自己的`README.md`里
![](http://img.zhufengpeixun.cn/addbadges.png)

## 7.提交并自动触发构建

![](http://img.zhufengpeixun.cn/buildsuccesses.png)

- 在README.md里就可以看到链接了,点击可以说[覆盖率详情页](https://coveralls.io/github/zhufengnodejs/zhufengreactui?branch=master)

![](http://img.zhufengpeixun.cn/coverage100.png)

